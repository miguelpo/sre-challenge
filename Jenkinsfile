pipeline {
    agent {
        label 'linux&&deploy&&aws'
    }

    options {
        skipDefaultCheckout()
    }

    stages {
        stage('Cleanup workspace') {
            steps {
                cleanWs()
            }
        }

        stage('Checkout repository and load variables') {
            steps {
                checkout scm
                load "vars.groovy"
            }
        }

        stage('Build the image and push to Harbor') {
            steps {
                docker.withRegistry("${registryUrl}","${svcAccount}"){
                    def myImage = docker.build("${dockerImageTag}:${version}.${env.BUILD_ID}")
                    myImage.push()
                }
            }
        }

        stage('Download helm chart') {
            steps {
              dir('chart') {
                git url: chartRepoGitUrl,
                    credentialsId: gitCred,
                    branch: 'master'
              }
            }
        }

        stage('Install helm chart in dev') {
            steps {
                withCredentials([file(credentialsId: k8sConnectionCredentialId, variable: 'KUBECONFIG'),]) {
                  sh "./bin/helm upgrade -i apisample ./chart --kube-context \"${k8sContext_dev}\" --namespace \"${k8sNamespace}\""
                }
            }
        }
        stage('Tests dev and coverage') {
            parallel {
                stage('Test dev') {
                    steps {
                        echo "Run some tests"
                    }
                    post {
                        always {
                            junit 'build/reports/dev/*.xml'
                        }
                    }
                }
                stage('Coverage') {
                    steps {
                        echo "SonarQube"
                    }
                }
            }
        }
        stage('Install helm chart in stg') {
            steps {
                withCredentials([file(credentialsId: k8sConnectionCredentialId, variable: 'KUBECONFIG'),]) {
                  sh "./bin/helm upgrade -i apisample ./chart --kube-context \"${k8sContext_stg}\" --namespace \"${k8sNamespace}\""
                }
            }
        }
        stage('Tests stg') {
            parallel {
                stage('Smoke test stg') {
                    steps {
                        echo "Run some tests"
                    }
                    post {
                        always {
                            junit 'build/reports/stg/smoke/*.xml'
                        }
                    }
                }
                stage('E2e test stg') {
                    steps {
                        echo "Run some tests"
                    }
                    post {
                        always {
                            junit 'build/reports/stg/e2e/*.xml'
                        }
                    }
                }
            }
        }
        stage('Install helm chart in prd') {
            steps {
                withCredentials([file(credentialsId: k8sConnectionCredentialId, variable: 'KUBECONFIG'),]) {
                  sh "./bin/helm upgrade -i apisample ./chart --kube-context \"${k8sContext_prd}\" --namespace \"${k8sNamespace}\""
                }
            }
        }
    }
    post { 
        always { 
            echo "Send notification to Slack/teams..."
        }
    }
}
