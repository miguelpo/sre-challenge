# README.MD

I have assumed all these things for this challenge:  
1.- The k8s cluster is already deployed (if not it could be deployed with Terraform using the module for EKS or if we want something simpler, we can use kops or eksctl)  
2.- The Jenkins slave is Linux and it has installed Docker and Helm.  
3.- The solution is on this repository but it should be in the original repository for working. The Helm chart can be in the same repository or uploaded to Harbor.  

## Pipeline
I haven't implemented a unit test stage, because I consider that it should be done before the build stage or with the multi-stage in the Dockerfile.
The Pipeline has these stages:  
1.- Cleanup workspace  
2.- Checkout repository and load variables  
3.- Build the image and push to Harbor  
4.- Download helm chart  
5.- Install helm chart in dev  
6.- Tests dev and coverage  
7.- Install helm chart in stg  
8.- Tests stg  
9.- Install helm chart in prd  

We should configure the integration between GitHub or Bitbucket and Jenkins. When a developer merges a branch (after pass the unit tests, coverage and other metrics), the pipeline starts.
## Observability
I would use a stack of Prometheus (metrics), ELK (logs), Instana (APM), Opsgenie (Notification management).
Also, I would use Runscope for monitoring the API.
If we add a good dashboard with the Golden Signals (latency, traffic, errors and saturation) and some saved searches or dashboards in Kibana, we will have a good observability of the application. Indeed we can use Kibana for make a quick and easy debug using the logs.
Finally, we should set up some alerts to warn us if something goes wrong or gets worse. The alerts can be configured with Prometheus via AlertManager, Runscope or even in Grafana. All the alerts should be sent to Opsgenie for deciding what we have to do with the alert (E.g: activate the on-call service or not).

## High Availability
As I have deployed the application in k8s cluster, I would use an HPA for the application. So, the application will scale up or down on the basis of one metric, that it can be cpu, memory or a custom metric. In this case, I would have to do some load tests for knowing which metric is better. Also, I would deploy an ingress (for getting access from outside the cluster) with a service (for routing the requests to the pods).
Also, if we want to improve the performance (E.g: reducing delay times associated with closing and reopening TCP connections and reduce server response time using cached content), we can use a CDN for delivering the content.

## Security
In terms of security, I would use certificates for the ingress and https. Also, I would use Vault to manage the secrets (certificates, password,...).
Regarding the pipeline and the docker images, I would add one more step or a job for scanning the vulnerabilities in the docker images. In this case, Aqua can be a good tool.

## Risks and alternatives
I see one main risk in the pipeline, which is the need for a big maturity in all stages (from code to test). If the quality of tests is not good, it can't be deployed in production without approval.
Other alternatives could be deployed releases every week or two weeks in production and using canary deployments.